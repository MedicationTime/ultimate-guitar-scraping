from bs4 import BeautifulSoup
import requests
import pandas as pd
import sys, os, getopt
import pickle
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.common.exceptions import NoSuchElementException

def getURLs ( base_url, end_of_url, nb_of_pages ) :

	song_names = []
	band_names = []
	tab_urls = []
	data = []

	delimiters = [ "[\'\"song_name\"", "\"artist_name\"", "\"tab_url\"" ]
	tabs_delimiter = "\"tabs\":[{"
	song_name_delimiter = "\"song_name\""
	artist_name_delimiter = "\"artist_name\""
	tab_url_delimiter = "\"tab_url\""

	for p in range( nb_of_pages ) :
		url = base_url + str( p + 1 ) + end_of_url 
		print( "Getting info at " + url )

		page = requests.get( url ).text
		# Extract part of the string with tabs data
		start = page.find( "\"tabs\":[" )
		end = page.find( ",\"hits\":" )
		tabs_data = page[ start + len( tabs_delimiter ) : end ].split( "{\"id\"" )
	

		for i in range( len( tabs_data ) ) :
			info = tabs_data[ i ].split( "," )
			## get info per tablature
			for j in range( len( info) ) : ## parse tablature info
				line = info[ j ].split( ":" )
				if line[ 0 ] == song_name_delimiter :
					song_name = line[ 1 ].replace( '"', '' )
					song_names.append( song_name )
					#print( song_name )
				if line[ 0 ] == artist_name_delimiter : 
					band_name = line[ 1 ].replace( '"', '' )
					band_names.append( band_name )
					#print( band_name )
				if line[ 0 ] == tab_url_delimiter : 
					tab_url = ( line[ 1 ] + ':' + line[ 2 ] ).replace( '"', '' )
					tab_url.replace( '\\', '' )
					tab_urls.append( tab_url )
					#print( tab_url )

#		print( song_names[ 0 ]+ " | " + band_names[0] + " | " + tab_urls[0] )
#		print( str( len( band_names ) ) + " " + str( len( song_names ) ) + " " + str( len( tab_urls ) ) )					

	bandsInfo = pd.DataFrame( { 'Artist' : band_names, 'Song' : song_names, "URL" : tab_urls } )	

	return bandsInfo

def login( driver ) :

	login = False
	found_login = ''
	print( "------ Start Login operation ------" )
	wait = WebDriverWait(driver, 10)
	## first element with this xpath is register, we need the second one which is the loggin
	#while( login == False ) :
	login_btn = driver.find_elements_by_xpath( "//button[@class='ggw-g _1Oybz Lgzwb eCjBm']" )
	login_btn[ 1 ].click()
	wait.until( EC.presence_of_element_located( ( By.XPATH, "//section[@class='_2mRC- _3fs6_']" ) ) )
	driver.find_element_by_name( 'username' ).send_keys( 'loicreboursiere@gmail.com' )
	driver.find_element_by_name( 'password' ).send_keys( 'Lilac&Wine05&&' )
	driver.find_element_by_xpath( "//button[@class='ggw-g _1iU2Q _2HOed _2aipe eCjBm']" ).click() 
	wait.until( EC.presence_of_element_located( ( By.XPATH, "//a[@href='http://profile.ultimate-guitar.com/loicreboursie1/']" ) ) )
	#	if( found_login ) :
	#		login = True

	print( "------ End Login operation : ------" )

def downloadTab( url, driver, db_path ) :

	print( "URL to download from " + url )
	#try :
	driver.get( url )
#	except TimeoutException :
#		print( "Skipping URL : Timeout while trying to get " + url )
#		return

	actions = ActionChains( driver )
	downloaded = False
	download_btn_xpath = "//button[@class='ggw-g _2PwLX _3jqeP eCjBm']"
	
	## 4 refers to band name position when splitting and [:-1] removes \\ characters
	folder_name = ( ( url.split( '/' ) )[ 4 ] )[ :-1 ]

	while( downloaded == False ) :
		
		try :
			actions = ActionChains( driver )
			actions.key_down( Keys.PAGE_DOWN )
			actions.perform()
		#	actions.key_down( Keys.PAGE_DOWN )
		#	actions.perform()
			try :
				download_btn = driver.find_element_by_xpath( download_btn_xpath )
			except NoSuchElementException :
				return


			if( download_btn.text == "DOWNLOAD GUITAR PRO TAB") :
				download_btn.click()
				print( "Download found and clicked")
			#	download_btn = driver.find_element_by_xpath( download_btn_xpath )
			#	download_btn.click()
			else : 
				print( "Download button not found")
#			if( )
#			download_btn.click()
			downloaded = True

		except UnexpectedAlertPresentException as e:
			
			print( "Exception found " + str( e ) + "\nShould now hit ENTER and downloadTab" )
			alert = driver.switch_to.alert
			print("Alerte : " + alert.text )
			alert.accept()
			downloaded = False	

	# Create band folder if not already done and move downloaded file to that place
	if( downloaded == True ) :
		if( db_path[ len( db_path ) - 1 ] == "/" ) :
			if( os.path.isdir( db_path + folder_name ) == False ) :
				os.mkdir( db_path + '/' + folder_name, 0o777 )
			else :
				print( "Folder already exists, no creation needed." )
		else :
			if( os.path.isdir( db_path + '/' + folder_name ) == False ) :
				os.mkdir( db_path + '/' + folder_name, 0o777 )
			else :
				print( "Folder already exists, no creation needed." )
	
#	os.system('mv ~/Downloads/'+ band.title() + '* '+  db_path + folder_name )	 


def main ( argv ) :
	base_url  = "https://www.ultimate-guitar.com/explore?page="
	nb_of_pages = 20
	end_of_url = "&type=Pro"
	
	file_csv = 'gptabsinfo.csv'
	file_txt = 'gptabinfo.txt'
	file_pickle = 'gptabsinfo.pkl'

	driver_name = ''
	driver_path = ''
	db_path = ''
	start = 0

	try:
		opts, args = getopt.getopt( argv, "hn:p:d:" , ["driver_name=", "driver_path=", "db_path="] )
		print( "args : " + str( args ) )
		if( len( args ) == 0 ):
			print( 'NO ARGUMENTS : scraping_0.2.py -n <driver_name> -p <driver_path -d <db_path>' )	
	except getopt.GetoptError:
		print( 'EXCEPT scraping_0.2.py -n <driver_name> -p <driver_path -d <db_path>' )
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print( 'scraping_0.2.py -n <driver_name> -p <driver_path -d <db_path>' )
			sys.exit()
		elif opt in ("-n", "--driver_name"):
			driver_name = arg
		elif opt in ("-p", "--driver_path"):
			driver_path = arg
		elif opt in ("-d", "--db_path"):
			db_path = arg
	
	#print( driver_name + " " + driver_path + " " + db_path )

	if( driver_name == 'Chrome' ) :	
		driver = webdriver.Chrome( driver_path )
	else :
		driver = ''
	
	if( os.path.isfile( file_pickle ) == False ) :
		# Getting data
		bandsInfo = getURLs( base_url, end_of_url, nb_of_pages, )
		# Writing data
		bandsInfo.to_pickle( file_pickle )
		bandsInfo.to_csv( file_csv, sep='\t', encoding='utf-8')
	#	print( bandsInfo )
	else :
		print( "file already exists, loading everything" )
		with open('start.pkl', 'rb') as f :
			start = pickle.load( f )
			print( "Starting tab scraping at " + str( start ) )
		with open( 'gptabsinfo.pkl', 'rb' ) as info :
			bandsInfo = pd.read_pickle( info )
		#	print( bandsInfo )


	urls = bandsInfo.get( 'URL')

	driver.get( urls[ 0 ].replace( '\\', '' ) )
	wait = WebDriverWait(driver, 10)
	wait.until( EC.presence_of_element_located( ( By.XPATH, "//button[@class='ggw-g _1Oybz Lgzwb eCjBm']" ) ) )
	## login is necessary to download tabs
	login( driver )

	for i in range ( len( urls ) - start ) :
		u = urls[ i + start ].replace( '\\', '' )
		downloadTab( u, driver, db_path )
		start += 1
		with open( 'start.pkl', 'wb' ) as f: 
			print( " start " + str( start ) )
			pickle.dump( start, f)


if __name__=='__main__':
	main( sys.argv[ 1: ] )