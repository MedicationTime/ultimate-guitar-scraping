import requests
import re
from bs4 import BeautifulSoup
import pandas as pd
import cgi
import shutil
import os
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import UnexpectedAlertPresentException


ultimate_URL = 'https://www.ultimate-guitar.com/'
alphabetical_listing = [ '0-9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ]
bandsName = []
bandsURLs = []
#global bands_info

def removeTags(string):
	'''
	Function to remove html tags
	'''
	return re.sub('<[^<]+?>', '', string)
 
 
def getBandTree(band, page):
	'''
	Function to get xml tree given the band name
	'''
	if type(page) == int:
		page = str(page)
	theURL = 'https://www.ultimate-guitar.com/search.php?band_name=' + band + \
		'&type%5B4%5D=500&rating%5B4%5D=5&approved%5B1%5D=1&page=' + page + \
		'&view_state=advanced&tab_type_group=text&app_name=ugt&order=myweight'
 
	pageBand = requests.get(theURL)
	return BeautifulSoup(pageBand.content, "lxml" )
	
def downloadTab( actions, driver ) :
	downloaded = False
	while( downloaded == False ) :
		try :
			actions.key_down(Keys.PAGE_DOWN).key_down(Keys.PAGE_DOWN).key_down(Keys.PAGE_DOWN)
			actions.perform()
			btn_download = driver.find_element_by_class_name("prosubmit")
			print( btn_download )
			btn_download.click()
			downloaded = True
		except UnexpectedAlertPresentException as e:
			print( "Exception found " + str( e ) + "\nShould now hit ENTER and downloadTab" )
			alert = driver.switch_to.alert
			print("Alerte : " + alert.text )
			alert.accept()
			downloaded = False	

def getAlphabeticalBandsURL( url ) : 
	content = requests.get( url )
	soup = BeautifulSoup( content.text, "lxml" )
	links = []
	
	for link in soup.findAll('a', {'class': "wb"}) :
		links.append(link.get('href'))
		
	print( str( len( links ) ) + " - " + url )
	
	return links

def getBsPageContent( url ) :
	return BeautifulSoup( requests.get( url ).text.strip(), "lxml" )

## Extract URLs of all alphabetical pages
def getAllPagesLinks( main_URL ) :
	URLs = []
	for letter in alphabetical_listing :
		page_URL = ultimate_URL + '/bands/' + letter + '.htm'
		letter_page = getBsPageContent( page_URL )
		nb_of_pages = len( letter_page.find_all( 'a', { "class" : "ys" } ) ) - 1
		print( str( nb_of_pages ) + " sub pages for letter " + letter )
		
		URLs.append( page_URL )
		for i in range( nb_of_pages ) :
			page_URL = ultimate_URL + '/bands/' + letter + str( (i+2) ) + '.htm'
			URLs.append( page_URL )
		
	return URLs

## Extract bands name and URL from a page of one specific letter 	
def getBandsInfo( url ) :
	main_page = getBsPageContent( url )
	nb_of_pages = len( main_page.find_all( 'a', { "class" : "ys" } ) ) - 1
	all_links = main_page.find_all( 'a' ) 	
	artist_links = [ i for i in all_links if i.get( 'href' ).startswith( '/artist/' ) ]
	# Only filter URL /artist/ff/sf and make them start with https://www.ultimate-guitar.com
	scrapped_url = [ ultimate_URL + artist.get( 'href' ) for artist in artist_links ]
	# Get text between <a> and </a> tags, remove HTML tags	
	scrapped_name = [ re.sub('<[^<]+?>', '', str( artist ) ) for artist in artist_links ]
	# Remove ' Tabs' from bands name
	scrapped_name = [ re.sub(' Tabs', '', str( band ) ) for band in scrapped_name ]
	# Concatenate name and URL
	return scrapped_name, scrapped_url #pd.DataFrame( { 'Name': bandsName, 'URL': bandsURL } )	
		
def getBandsTabURL( url ) :

	band_page = getBsPageContent( url )	
	all_links = band_page.find_all( 'a' ) 	
	URLs = [ i for i in all_links if i.get( 'href' ).startswith( 'https://tabs.ultimate-guitar.com/' ) ]	
		
	return URLs


		
def retrieveAllTabsURL ( main_URL ) :
	global bandsName, bandsURLs
	gp = 'guitar_pro'
	gptab = []
	txt_filename = "gptabs-all.txt"
	
	print( '---------  Gathering letter subpages information ---------' )
	all_letter_pages_URL = getAllPagesLinks( main_URL )
	
	print( '---------  Gathering bands information ---------' )

	for page in all_letter_pages_URL :
		print( 'Gathering bands name in ' + str( page ) )
		name, url = getBandsInfo( page )
		bandsName += name 
		bandsURLs += url 
		
	bandsInfo = pd.DataFrame( { 'Name' : bandsName, 'URL' : bandsURLs } )
	
	print( '---------  Gathering songs URL and writing them in' + txt_filename + '---------' )
	with open( txt_filename, "w" ) as txt :
		for i in range( len( bandsInfo ) ) : 
			print( "here ??????" )
			tabs = getBandsTabURL( bandsInfo.ix[ i ].get( 'URL' ) )
			songs_url = [ tab.get( 'href' ) for tab in tabs ]
			print( songs_url )
			name = bandsInfo.ix[ i ].get( 'Name' )
			for u in songs_url :
				print( "##### Checking URL " + str( u ) )
				page = getBsPageContent( u )
			#	all_links = page.fin_all( 'a' )
			#	tab_link = [ i for i in all_links if i.get( 'href' ).startswith( 'https://tabs.ultimate-guitar.com/' ) ]
				if ( gp in u ) == True :
					print( "%%%%% Guitar Pro Tabs " + u )
					gptab.append( u )
					txt.write( "%s %s\n" % ( name, u ) )	
					print( str( name ) + " " + str( u )  )
				else :
					print( "No guitar pro tab" )

	txt.close()	
    	
def getBandsTabs( url )	 
	page = 1
	if( os.path.isdir( database_path + band ) == False ) :
		os.mkdir( database_path + band, 0o777 )
	else :
		print( "Folder already exists, no creation needed." )
	
	url = getBandTree(band, page)
	
	#Get max pages
	pages = bandURL.find_all('div', { "class" : "paging" })
	maxPages = str(pages).count('')
	dfs = []

	print( "There is " + str( maxPages ) + " for " + band )

	for page in range(1, maxPages + 1):
		url = getBandTree(band, page)
		mybs = url.find_all('b', { "class" : "ratdig" })
		mybs2 = [removeTags(str(rating)) for rating in mybs]	  
		songname = url.find_all('a', { "class" : "song result-link js-search-spelling-link" })
		songname2 = [re.sub('([\(\[]).*?([\)\]])', '', removeTags(str(song)).strip()).strip() for song in songname]
		tabType = url.find_all('strong')
		tabType2 = [removeTags(str(tab)) for tab in tabType]
		
		if len(tabType2) > len(mybs2):
			df1 = pd.DataFrame({'Rating': mybs2, 'Type': tabType2[:len(mybs2)]})
		
		df2 = df1[df1.Type == 'guitar pro']
		df2.loc[:,'Song_Name'] = songname2[:len(df2)]
		links = []
		print( "Number of tablature found for "  + band + " for page " + str( page ) + " : " + str( len( songname ) ) )
		tab_index = 0
		for a in songname:
			links.append(a['href'])
			driver.get( str( a['href'] ) )
			actions = ActionChains( driver )
			downloadTab( actions, driver )
			tab_index += 1
			print( 'Downloading tablature ' + str( tab_index ) + " " + a['href'] )
				
		os.system('mv ~/Downloads/'+ band.title() + '* '+  database_path+band )	   	

def main():
	global_bands_url = "https://www.ultimate-guitar.com/bands"
	ultimate_URL = "https://www.ultimate-guitar.com/"
	runsikulix_path = "/Users/Loic/Documents/sikulix/runsikulix"
	database_path = "/Volumes/DATA/Database/GuitarProTablature/"
	chrome_driver_path = "/Volumes/DATA/Recherche/Developpement/website_automation/selenium/webdriver/chromedriver"	
	band = 'oasis'

	#driver = webdriver.Chrome( chrome_driver_path )
	
	retrieveAllTabURL( ultimate_URL )
	
	
	#	try : 	
	#		df2.loc[:,'Song_Links'] = links[:len(df2)]
	#	except ValueError :
	#		print( "Problème dans la copie entre !" ) 
			
	#	df3 = df2.loc[df2.groupby(['Song_Name'], sort=False)['Rating'].idxmax()]
	#	dfs.append(df2)
		#os.system(runsikulix_path + " -r ~/Documents/sikulix/downloadGPTab/downloadGPTab.sikuli")
		
		
	#tot_df = pd.concat(dfs)
	#driver.quit()
#	print(tot_df)
 
#	song_links_list = list(tot_df.Song_Links)
#	for i in range(len(tot_df)):
#		webPage = requests.get(song_links_list[i])
#		soup = BeautifulSoup(webPage.content)
#		tab_id = soup.find_all("input", {"type" : "hidden", "name" : "id", "id" : "tab_id"})
#		the_id = tab_id[0].get('value')
#		mydir = os.path.dirname(os.getcwd() + '/out/' +band + '/')
#		print( mydir )
#		if not os.path.exists(mydir):
#			os.makedirs(mydir)

	
		
#		filepath = download_url('https://tabs.ultimate-guitar.com/tab/download?id='+str(the_id), mydir)

 
if __name__=='__main__':
	main()